# **UI-Automation-Framework**

## **1. Introduction**
The IU test automation framework was created in order to execute automated test cases for Globant Test.

You only need to download the project and run it (Steps explained in future topics). The results generated by this framework are:

1. HTML report
2. Logs

Requirements you need in your computer:
- Admin rights on your computer to download and install apps.
- Chocolatey package manager (only windows and optional)
- Homebrew (MAC and optional)
- Java JDK >1.8
- Gradle 4.9
- IntelliJ, Eclipse Luna or above or another java IDE.

## **2. Tools**
the tools and programming language used on this project are:

1. Java as Programming Language
2. TestNG as Unit Test Framework
3. Gradle as Tool for compiling and execution
4. Git as control version software

## **3. Steps to Execute**

### 3.1 Installing Java, Gradle and Git
If you have installed Java (1.8 or above) and Gradle (4.6 or above), skip this section

**Windows**
**Chocolatey installation**

Visit https://chocolatey.org/install and follow the installation steps.

**Java Installation**
1. Open a command line as administrator.
2. Copy and paste: ```choco install jre8 -y```
3. Wait for the command to complete. If you don't see any errors, you are ready to use Java 

**Gradle Installation**
1. Open a command line as administrator.
2. Copy and paste: ```choco install gradle -y```
3. Wait for the command to complete. If you don't see any errors, you are ready to use Gradle

**Git Installation**
1. Open a command line as administrator.
2. Copy and paste: ```choco install git -y```
3. Wait for the command to complete. If you don't see any errors, you are ready to use Git

**MAC**
**Homebrew installation**
Visit https://docs.brew.sh/Installation and follow the installation steps.

**Java Installation**
Execute the following command on terminal
```
brew update
brew install java
```

**Gradle Installation**
Execute the following command on terminal
```
brew update
brew install gradle
```

**Install Git**
Execute the following command on terminal
```
brew update
brew install git
```

### 3.2 Executing the framework
1. To get started, first clone the repo from https://bitbucket.org/gsanchezm/globant-demo.git, you can use the comand ```git clone <repo name>```
2. Open you IDE  
3. All depencencies must be downloaded
4. Open a terminal o command line
5. Execute `gradle test`
6. You can see the execution in real time using Zalenium on http://http://178.62.91.86:4444/grid/admin/live
7. Wait for test execution to finish.
8. You can see one redord of your execution on: http://http://178.62.91.86:4444/dashboard/#

## **4. Technology Stack**

### 4.1 Selenium
Selenium automates browsers. That's it! What you do with that power is entirely up to you. Primarily, it is for automating web applications for testing purposes, but is certainly not limited to just that. Boring web-based administration tasks can (and should!) be automated as well.

Selenium has the support of some of the largest browser vendors who have taken (or are taking) steps to make Selenium a native part of their browser. It is also the core technology in countless other browser automation tools, APIs and frameworks.

### 4.2 Selenium Grid
Selenium Grid is a part of the Selenium Suite that specializes in running multiple tests across different browsers, operating systems, and machines in parallel.

### 4.3 Java
Java is a general-purpose computer-programming language that is concurrent, class-based, object-oriented and specifically designed to have as few implementation dependencies as possible. It is intended to let application developers "write once, run anywhere" (WORA), meaning that compiled Java code can run on all platforms that support Java without the need for recompilation. Java applications are typically compiled to bytecode that can run on any Java virtual machine (JVM) regardless of computer architecture.

### 4.4 Cucumber
Cucumber is a tool that supports Behavior Driven Development (BDD). It offers a way to write tests that anybody can understand, regardless of their technical knowledge.

In BDD, users (business analysts, product owners) first write scenarios or acceptance tests that describes the behavior of the system from the customer's perspective, for review and sign-off by the product owners before developers write their codes.

### 4.5 Gherkin
Gherkin is a simple, lightweight and structured language which uses regular spoken language to describe requirements and scenarios. By regular spoken language we mean English, French and around 30 more languages.

As Gherkin is a structured language it follows some syntax let us first see a simple scenario described in gherkin.

```gherkin
Feature: Search feature for users
This feature is very important because it will allow users to filter products

Scenario: When a user searches, without spelling mistake, for a product name present in inventory. All the products with similar name should be displayed

Given User is on the main page of www.myshopingsite.com
When User searches for laptops
Then search page should be updated with the lists of laptops
```

### 4.6 Log4J
log4j is a reliable, fast and flexible logging framework (APIs) written in Java, which is distributed under the Apache Software License. log4j is a popular logging package written in Java. log4j has been ported to the C, C++, C#, Perl, Python, Ruby, and Eiffel languages. 

### 4.7 TestNG
TestNG is a testing framework inspired from JUnit and NUnit but introducing some new functionalities that make it more powerful and easier to use, such as:

- Annotations.
- Run your tests in arbitrarily big thread pools with various policies available (all methods in their own thread, one thread per test class, etc...).
- Test that your code is multithread safe.
- Flexible test configuration.
- Support for data-driven testing (with @DataProvider).
- Support for parameters.
- Powerful execution model (no more TestSuite).
- Supported by a variety of tools and plug-ins (Eclipse, IDEA, Maven, etc...).
- Embeds BeanShell for further flexibility.
- Default JDK functions for runtime and logging (no dependencies).
- Dependent methods for application server testing.

TestNG is designed to cover all categories of tests:  unit, functional, end-to-end, integration, etc... 

### 4.8 Extent Reports
Extent Report is a HTML reporting library for Selenium WebDriver for Java which is to a great degree simple to use and makes excellent execution reports. We can use this tool within our TestNG automation framework. As an automation tester its obligation to catch great reporting and present to administration group.

### 4.9 Gradle
Gradle is an open-source build automation tool focused on flexibility and performance. Gradle build scripts are written using a Groovy or Kotlin DSL. 

## **5. Framework Architecture**

### 5.1 Folder Structure

Inside src >main >java >com >imdb; you will find the package **framework**, inside, all the reusable clases, enums and interfaces in order to use on diferent projects

In the folder src >test >java, all the info related to the application is found, like features, pages and step definitions.

**Top - Level Directory Layout**

    .
    ├── drivers                        # Drivers to communicate with browsers like Chrome, Firefox, etc.
    ├── results                        # Files generated after execution (HTML, Logs, screenshots)
    ├── src                            # Source files (alternatively `doc`)
    │   ├── main                       
    |       ├── java
    |           ├── com
    |               ├── imdb
    |                   ├── framework
    |                       ├── base      # Parent classes to create other classes 
    |                       ├── config    # Constants and configuration files
    |                       ├── controls  # Custom wrapper files
    |                       ├── utils     # Tools and utilities
    |   ├── test                        # Automated tests
    |       ├── java
    |           ├── features              # Cucumber features files
    |           ├── pages                 # Pages classes 
    |           ├── runner                # Interfaces and class to execute using TestNG
    |           ├── steps                 # Step definition classes
    |       ├── resources                 # External files
    ├── .gitignore                                         
    ├── build.gradle                   # File with dependencies
    ├── extent-config.xml              # Configuration xml for HTML report
    ├── jenkinsfile                    # Configuration file for jenkins CI/CD
    └── README.md

**Framework Directory**

    .
    ├── ...
    ├── framework                   
    │   ├── base                    
    |       ├── Base.java            # Class to load the webelements factory
    │       ├── BasePage.java        # Class to obtain the page class
    │       ├── BrowserType.java     # Enum with browser's name (chrome, firefox, etc.) 
    │       └── DriverFactory.java   # Class to create, obtain and remove the WebDriver object       
    │   ├── config         
    │       ├── Constants.java       # Constant like URL, tpath to generate HTML report, logs, etc.
    │       └── Log4j.properties     # File with patter to display logs
    │   ├── controls                
    |       ├── api                  # Classes and interfaces to communicate with Factory pattern
    │       ├── elements             # Classes and interfaces with custom WebElement controls
    │       └── internals            # Classes and interface with methods like click(), getText(), etc.
    │   ├── utils                
    │       ├── CucumberUtils.java   # Cucumber utils like get info from table
    │       └── SeleniumUtils.java   # Utils like highlight and object and takescreenshot on error
    └── ...
    

### 5.2 Architecture Diagram

**Framework**
Test Automation Framework (TAF) provide platform independent testing solution for automating web business processes.

![image](https://user-images.githubusercontent.com/24705055/46901585-3bd6ce80-ce7c-11e8-8536-f5901f9dadd4.png)

**Test**
The framework used to create test cases, is a combination of Cucumber and selenium webdriver scripts

![image](https://user-images.githubusercontent.com/24705055/45114706-4ed6e000-b113-11e8-8cb4-67b619f2544a.png)

### **5.3 Continuous Testing Schema**
In the image below, you will find the CI/CD diagram

![image](https://user-images.githubusercontent.com/24705055/45061456-3363ca80-b06a-11e8-8ce0-c2e0cb13aced.png)

## **6. Script Development Management Process**
Please observe the following guidelines when working with GitHub/BitBucket.

### 6.1 Branch Naming
1.	Provide a name that describes the feature/service it relates to, as well the change being made using the following format feature-change. For example, rooms-initalPlacementScriptCreation

### 6.2 Pull Request Reviews
1.	Please review this [PR best practices blog](https://blog.github.com/2015-01-21-how-to-write-the-perfect-pull-request/).
2.	Aside from the aforementioned: 
    1.	Please discuss your review through Slack first with the author to avoid turning BitBucket history comments into a long chat thread.
    2.	After this, only register comments you feel they need to be kept as historical reference. Such as a decision made by requirements, common agreements, etc.

### 6.3 PR Process

**Starting to work on a feature**
```
git checkout master
git pull origin master
git checkout -b your_branch_name (name your branches based on the activity to perform, such as "add_X_scripts")
(Start adding code)
```

**Getting branch ready for PR**
```
git pull --rebase origin master (to make sure you have the latest code from master branch)
git status (to verify staged/unstaged changes)
git add . (add all your changes)
git commit -m "Message explaining the changes" (such as "Add X scripts and modify Y scripts"... No past tense)
git push origin your_branch_name
(Create PR using BitBucket)
```

**Include requested changes within your open PR: Adjusting PR before approval** 
```
git pull --rebase origin master (in the case another PR was approved and merged while your PR was still open)
git status (to verify staged/unstaged changes)
git add . (add all your changes)
git commit --amend (to avoid creating a new commit but using the last one instead... Same message)
git push -f origin your_branch_name
(This action will modify the PR content and files changed)
```

**After PR has been approved**
``` 
Merge and commit (using BitBucket/Github when possible)
Remove remote branch (if done through BitBucket, option will prompt immediately)
git checkout master (to change to master branch)
git pull origin master (to get latest changes from your merge)
git branch -d your_branch_name
```

## **7. How to ...?**

### How to create a new element (web controller)?

You need to go under the package:

    .
    ├── ...
    ├── framework                   
    │   ├── controls                
    │       └── elements            # Classes and interface with methods like click(), getText(), etc.
    └── ...

- You must create an ElementBase class, this class needs to extend (Inheritance) from ControlBase and implements Button interface methods. The constructor must have _WebElement_ as argument.  

```java
public class ElementBase extends ControlBase implements Element {

    public ElementBase(WebElement element) {
        super(element);
    }
}
```
- Then, you can implement your custom method (override all the methods). 
- Highlight must be executed over the web elements (getWrappedElement), for these reason, you need to call the method _highLight_ from _SeleniumUtils_ class.
- You need to add the action that must be performed in the method.

```java
public class ElementBase extends ControlBase implements Element {

    public ElementBase(WebElement element) {
        super(element);
    }

    @Override
    public void customMethodOne() {
        SeleniumUtils.highLight(getWrappedElement());
        click();
    }

    @Override
    public String customMethodTwo() {
        SeleniumUtils.highLight(getWrappedElement());
        getText();
    }

    @Override
    public void customMethodN() {
        SeleniumUtils.highLight(getWrappedElement());
        submit();
    }
}
```
- It's time to create our Button interface. Must extends from Control.
- The interface needs to use the annotation _@ImplementeBy_ and add our ElementBase class as parameter.

```java
@ImplementedBy(ElementBase.class)
public interface Element extends Control {
}
```
- We need to implement our method previously created.

```java
@ImplementedBy(ElementBase.class)
public interface Element extends Control {

    void customMethodOne();
    String customMethodTwo();
    void customMethodN();
}
```

- We'll create **Button** controller so we need to create **ButtonBase** class.

```java
public class ButtonBase extends ControlBase implements Button {

    public ButtonBase(WebElement element) {
        super(element);
    }
}
```
- Now we're going to add our custom methods: _performClick()_, _getButtonText()_, _performSubmit()_.

```java
public class ButtonBase extends ControlBase implements Button {

    public ButtonBase(WebElement element) {
        super(element);
    }

    @Override
    public void performClick() {
        SeleniumUtils.highLight(getWrappedElement());
        click();
    }

    @Override
    public String getButtonText() {
        SeleniumUtils.highLight(getWrappedElement());
        return getText();
    }

    @Override
    public void performSubmit() {
        SeleniumUtils.highLight(getWrappedElement());
        submit();
    }
}
```
- Time to create our Button interface and implement our methods

```java
@ImplementedBy(ButtonBase.class)
public interface Button extends Control {

    void performClick();
    String getButtonText();
    void performSubmit();
}
```

### How to create a new page class?

Each page class must be created on:

    .
    ├── ...
    ├── test                   
    │   ├── java                
    │       └── pages    # Pages classes 
    └── ...

- You need to create your page class, this class must extends from _BasePage_. 

```java
public class TestPage extends BasePage {
}
```
- We are using the PageFactory automation design patter, so you need to use the annotation _@FindBy_ and call the right controller, for example if you want to locate an textbox, you should use _TextBox_ controller.

```java
public class TestPage extends BasePage {

    @FindBy(id = "username1")
    TextBox txtUserName;

    @FindBy(xpath = ".//button[@type='submit']")
    Button btnLogin;
}
```
- When all the web elements are mapped into the page class, you can add methods in order to interact with them.

```java
public class TestPage extends BasePage {

    @FindBy(id = "username1")
    TextBox txtUserName;

    @FindBy(xpath = ".//button[@type='submit']")
    Button btnLogin;

    public void setUserName(String userName){
        txtUserName.enterText(userName);
    }

    public void clickLogin(){
        btnLogin.click();
    }
}
```
We'll create a login page.

- Create the LoginPage class. 

```java
public class LoginPage extends BasePage {
}
```

- Let's add our web elements and their controllers.

```java
public class LoginPage extends BasePage {

    @FindBy(id = "username1")
    TextBox txtUserName;

    @FindBy(id = "password2")
    TextBox txtPassword;

    @FindBy(xpath = ".//button[@type='submit']")
    Button btnLogin;

}
```

- Now, it's time to add our methods, in this example we use the Fluent API in order to emphasize the purpose of our code.

```java
public class LoginPage extends BasePage {

    @FindBy(id = "username1")
    TextBox txtUserName;

    @FindBy(id = "password2")
    TextBox txtPassword;

    @FindBy(xpath = ".//button[@type='submit']")
    Button btnLogin;

    public LoginCommand loginAs(String userName){
        return new LoginCommand(userName);
    }

    public class LoginCommand{
        private String userName;
        private String password;

        public LoginCommand(String userName){
            this.userName = userName;
        }

        public LoginCommand withPassword(String password){
            this.password = password;
            return  this;
        }

        public void login(){
            txtUserName.enterText(userName);
            txtPassword.enterText(password);
            btnLogin.performSubmit();
        }

    }
}
```
### How to create a new feature file?

All the feature files must exist on:

    .
    ├── ...
    ├── test                   
    │   ├── java                
    │       └── features  # Cucumber features files
    └── ...

- The file extension must be .feature, this'll help cucumber to identify it.
- In section 4, we explain what is cucumber and gherkin, also one example is presented, now, we are going to create our Login feature file. in the example you'll see ihow the login test case could be implemented on the _Login.feature_ file.

```
Feature: Login in X Web Page

  Background:
    Given user must be on X web application

    Scenario: Login using valid credentials
      When I access with valid credentials as username and password on login page
        |username     |password|
        |super.admin1 |tester  |
      Then I should see the text "Mobile Apps"
```

### How to implement code on step definition class?

After one feature file is created, and scenarios are implemented, we need to create our step definition file, these files will be find on:

    .
    ├── ...
    ├── test                   
    │   ├── java                
    │       └── steps  # Step definition classes
    └── ...

- Base on the Login feature file, we'll create LoginSteps class, you must extend from Base class.

```
public class LoginSteps extends Base {
}
```

- You can run your feature file, and the methods will be generated automaticaly, then you only need to copy and paste it on your step class.

```java
public class LoginSteps extends Base {

    @Given("^user must be on X web application$")
    public void userMustBeOnXWebApplication() {
    }

    @When("^I access with valid credentials as username and password on login page$")
    public void iAccessWithValidCredentialsAsSuperAdminAndTesterOnLoginPage(DataTable data) {
    }

    @Then("^I should see the text \"([^\"]*)\"$")
    public void iShouldSeeTheText(String appsMobileTitle) {
    }
}
```

- Now, we are going to call our page class. Every time you need to change from one page class to other, you must use the variable _currentPage_ and equalize to the page class in the getInstance method.

```java
@Given("^user must be on X web application$")
    public void userMustBeOnXWebApplication() {
        //Initialize the page where you'll work
        currentPage = getInstance(LoginPage.class);
    }
}
```

- You are able to call the methods from that specific class using the _as(Page.class)_ method

```java
 @Given("^user must be on X web application$")
    public void userMustBeOnXWebApplication() {
        //Initialize the page where you'll work
        currentPage = getInstance(LoginPage.class);
    }

    @When("^I access with valid credentials as username and password on login page$")
    public void iAccessWithValidCredentialsAsSuperAdminAndTesterOnLoginPage(DataTable data) {
        CucumberUtil.convertDataTableToDic(data);
        currentPage.as(LoginPage.class)
                .loginAs(CucumberUtil.getCellValueWithRowIndex("username",1))
                .withPassword(CucumberUtil.getCellValueWithRowIndex("password",1))
                .login();
    }
```

- And you can complete all your steps definitions.

```java
public class LoginSteps extends Base {

    @Given("^user must be on X web application$")
    public void userMustBeOnXWebApplication() {
        //Initialize the page where you'll work
        currentPage = getInstance(LoginPage.class);
    }

    @When("^I access with valid credentials as username and password on login page$")
    public void iAccessWithValidCredentialsAsSuperAdminAndTesterOnLoginPage(DataTable data) {
        CucumberUtil.convertDataTableToDic(data);
        currentPage.as(LoginPage.class)
                .loginAs(CucumberUtil.getCellValueWithRowIndex("username",1))
                .withPassword(CucumberUtil.getCellValueWithRowIndex("password",1))
                .login();
    }

    @Then("^I should see the text \"([^\"]*)\"$")
    public void iShouldSeeTheText(String appsMobileTitle) {
        //must change to Apps page
        currentPage = getInstance(AppsPage.class);
        System.out.println(currentPage.as(AppsPage.class).getMobileAppsTitle());
        Assert.assertEquals(appsMobileTitle, currentPage.as(AppsPage.class).getMobileAppsTitle());
    }
}
```
## **8. Results**

### 8.1 HTML Report

![image](https://user-images.githubusercontent.com/24705055/46901719-1f3b9600-ce7e-11e8-9955-5cf4842d106f.png)

### 8.2 Logs

![image](https://user-images.githubusercontent.com/24705055/45114740-68782780-b113-11e8-87e7-4a849c39c26c.png)

### 8.3 Screenshot on Error

![image](https://user-images.githubusercontent.com/24705055/46901688-a50b1180-ce7d-11e8-87d3-26170acbd3ea.png)

![image](https://user-images.githubusercontent.com/24705055/46901694-b8b67800-ce7d-11e8-8196-c0507f0e258e.png)