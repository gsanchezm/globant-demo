package pages;

import com.imdb.framework.base.BasePage;
import com.imdb.framework.base.DriverFactory;
import com.imdb.framework.controls.elements.HyperLink;
import com.imdb.framework.controls.elements.TextBox;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {
    @FindBy(linkText = "Other Sign in options")
    HyperLink lnkOtherSignIn;

    @FindBy(id = "nbusername")
    HyperLink lnkUserName;

    @FindBy(linkText = "Log Out")
    HyperLink lnkLogOut;

    @FindBy(xpath = "(//span[@class='downArrow'])[6]")
    WebElement lstUserMenu;

    @FindBy(id = "navbar-query")
    TextBox txtSearch;

    public void clickOtherSignInOptions(){
        lnkOtherSignIn.clickLink();
    }

    public String getUserName(){
        return lnkUserName.getUrlText();
    }

    public void moveCursorToUserName(){
        lstUserMenu.click();
    }

    public void clickLogOut(){
        lnkLogOut.clickLink();
    }

    public void setSearch(String search){
        txtSearch.enterText(search);
    }
}
