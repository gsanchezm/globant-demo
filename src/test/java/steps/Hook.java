package steps;

import com.imdb.framework.base.DriverFactory;
import com.imdb.framework.config.Constants;
import com.imdb.framework.utils.SeleniumUtils;
import com.imdb.framework.utils.WaitUtil;
import com.vimalselvam.cucumber.listener.Reporter;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import gherkin.formatter.model.Result;
import org.openqa.selenium.remote.LocalFileDetector;

import java.io.IOException;

public class Hook extends DriverFactory {

    @Before
    public void initialize(Scenario scenario) {
        Reporter.addScenarioLog(scenario.getName());
        getInstance().getDriver().navigate().to(Constants.URL);
        WaitUtil.sync();
    }

    @After
    public void tearDown(Scenario scenario) throws IOException {
        if (scenario.getStatus().equals(Result.FAILED)) {
            Reporter.addScreenCaptureFromPath(SeleniumUtils.takeSnapShot(),"Test_Failed!");
        }
    }
}
