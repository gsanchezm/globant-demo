package steps;

import com.imdb.framework.base.Base;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import pages.HomePage;
import pages.LoginPage;

public class SignInSteps extends Base {
    @Then("^I login with user \"([^\"]*)\" and pass \"([^\"]*)\"$")
    public void iLoginWithUserAndPass(String user, String password)  {
        currentPage = getInstance(LoginPage.class);
        currentPage.as(LoginPage.class).loginAs(user).withPassword(password).login();
    }

    @And("^I Log Out from the page$")
    public void iLogOutFromThePage() throws Throwable {
        currentPage = getInstance(HomePage.class);
        currentPage.as(HomePage.class).moveCursorToUserName();
        currentPage.as(HomePage.class).clickLogOut();
    }
}
