package runner;

import com.imdb.framework.base.BrowserType;
import com.imdb.framework.base.DriverFactory;
import com.imdb.framework.config.Constants;
import com.vimalselvam.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.CucumberFeatureWrapper;
import cucumber.api.testng.TestNGCucumberRunner;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.remote.LocalFileDetector;
import org.testng.ITestContext;
import org.testng.annotations.*;
import com.vimalselvam.cucumber.listener.ExtentProperties;

import java.io.File;
import java.net.MalformedURLException;

import static com.vimalselvam.cucumber.listener.Reporter.loadXMLConfig;

@CucumberOptions(features = {"src/test/java/features/"},
        glue = {"steps"},
        plugin = {"com.vimalselvam.cucumber.listener.ExtentCucumberFormatter:"})
public class TestRunner {
    private TestNGCucumberRunner testNGCucumberRunner;
    private static Logger Log = Logger.getLogger(TestRunner.class);

    @BeforeClass(alwaysRun = true)
    @Parameters("browser")
    public void setUpClass(String browser) throws MalformedURLException {
        testNGCucumberRunner = new TestNGCucumberRunner(this.getClass());
        ExtentProperties extentProperties = ExtentProperties.INSTANCE;
        extentProperties.setReportPath(Constants.HTML_REPORT);
        PropertyConfigurator.configure(Constants.LOG_PRO_DIR);
        DriverFactory.getInstance().setDriver(BrowserType.valueOf(browser));
    }

    @Test(dataProvider = "features")
    public void runnerTests(CucumberFeatureWrapper cucumberFeatureWrapper) {
        testNGCucumberRunner.runCucumber(cucumberFeatureWrapper.getCucumberFeature());
    }

    @DataProvider
    public Object[] features(ITestContext context) {
        return testNGCucumberRunner.provideFeatures();
    }

    @AfterTest(alwaysRun = true)
    public void afterClass() {
        testNGCucumberRunner.finish();
        loadXMLConfig(new File(Constants.EXTENT_CONFIG));
        Reporter.setSystemInfo("user", System.getProperty("user.name"));
        Reporter.setSystemInfo("os", System.getProperty("os.name"));
        Reporter.setTestRunnerOutput("Sample test runner output message");
        DriverFactory.getInstance().removeDriver();
    }
}
