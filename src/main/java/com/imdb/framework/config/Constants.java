package com.imdb.framework.config;

public class Constants {
    public static final String LOG_PRO_DIR = System.getProperty("user.dir")+"/src/main/java/com/imdb/framework/config/log4j.properties";
    public static final String FILES_DIR = System.getProperty("user.dir")+"/src/test/resources/";
    public static final String SCREENSHOT_FOLDER = System.getProperty("user.dir")+"/results/screenshots/";
    public static final String URL = "https://www.imdb.com";
    public static final String HTML_REPORT = System.getProperty("user.dir") + "/results/ui_report.html";
    public static final String EXTENT_CONFIG = System.getProperty("user.dir") + "/extent-config.xml";
    public static final String GRID_PORT = "http://178.62.91.86:4444/wd/hub";
}
