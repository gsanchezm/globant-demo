package com.imdb.framework.controls.elements;

import com.imdb.framework.controls.api.ImplementedBy;
import com.imdb.framework.controls.internals.Control;

@ImplementedBy(TextBoxBase.class)
public interface TextBox extends Control {

    void enterText(String text);
    String getTextValue();

}
