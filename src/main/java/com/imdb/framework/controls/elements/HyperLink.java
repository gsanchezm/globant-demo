package com.imdb.framework.controls.elements;

import com.imdb.framework.controls.api.ImplementedBy;
import com.imdb.framework.controls.internals.Control;

@ImplementedBy(HyperLinkBase.class)
public interface HyperLink extends Control {


    void clickLink();
    String getUrlText();
    boolean checkUrlTextContains(String containsText);
}
