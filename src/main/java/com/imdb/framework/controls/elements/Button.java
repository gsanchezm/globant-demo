package com.imdb.framework.controls.elements;

import com.imdb.framework.controls.api.ImplementedBy;
import com.imdb.framework.controls.internals.Control;

@ImplementedBy(ButtonBase.class)
public interface Button extends Control {

    void performClick();
    String getButtonText();
    void performSubmit();
    boolean getButtonIsEnabled();
}
