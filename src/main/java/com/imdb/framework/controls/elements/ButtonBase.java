package com.imdb.framework.controls.elements;

import com.imdb.framework.controls.internals.ControlBase;
import com.imdb.framework.utils.SeleniumUtils;
import org.openqa.selenium.WebElement;

public class ButtonBase extends ControlBase implements Button {

    public ButtonBase(WebElement element) {
        super(element);
    }

    @Override
    public void performClick() {
        SeleniumUtils.highLight(getWrappedElement());
        click();
    }

    @Override
    public String getButtonText() {
        SeleniumUtils.highLight(getWrappedElement());
        return getText();
    }

    @Override
    public void performSubmit() {
        SeleniumUtils.highLight(getWrappedElement());
        submit();
    }

    @Override
    public boolean getButtonIsEnabled() {
        return isEnabled();
    }
}
