package com.imdb.framework.base;

public enum BrowserType {
    CHROME,
    IE,
    FIREFOX,
    SAFARI,
    EDGE
}
