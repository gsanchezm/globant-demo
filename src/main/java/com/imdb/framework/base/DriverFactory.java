package com.imdb.framework.base;

import com.imdb.framework.config.Constants;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class DriverFactory {
    private static DriverFactory instance = new DriverFactory();

    public static DriverFactory getInstance(){
        return instance;
    }

    ThreadLocal<RemoteWebDriver> driver = new ThreadLocal<RemoteWebDriver>();

    public RemoteWebDriver getDriver(){
        return driver.get();
    }

    public RemoteWebDriver setDriver(BrowserType browser) throws MalformedURLException {
        DesiredCapabilities caps = null;

        String getOS = System.getProperty("os.name").toLowerCase();
        String osName = "";
        if(getOS.contains("mac")){
            osName = "mac";
        }else if(getOS.contains("win")){
            osName = "windows";
        }else if(getOS.contains("nix") || getOS.contains("nux") || getOS.contains("aix")){
            osName = "linux";
        }

        String driverPath = System.getProperty("user.dir") + "/drivers/";

        switch (browser){
            case CHROME:
                if(osName.equals("windows")){
                    System.setProperty("webdriver.chrome.driver", driverPath + "chromedriver.exe");
                }else{
                    System.setProperty("webdriver.chrome.driver", driverPath + "chromedriver");
                }
                ChromeOptions options = new ChromeOptions();
                options.setCapability(CapabilityType.PLATFORM_NAME, Platform.ANY);
                caps = DesiredCapabilities.chrome();
                caps.setBrowserName("chrome");
                break;
            case  IE:
                if(osName.equals("windows")){
                    System.setProperty("webdriver.ie.driver", driverPath + "IEDriverServer.exe");
                }
                caps = DesiredCapabilities.internetExplorer();
                caps.setBrowserName("ie");
                break;
            case FIREFOX:
                if(osName.equals("windows")){
                    System.setProperty("webdriver.gecko.driver", driverPath + "geckodriver.exe");
                }else{
                    System.setProperty("webdriver.gecko.driver", driverPath + "geckodriver");
                }
                caps = DesiredCapabilities.firefox();
                caps.setBrowserName("firefox");
                break;
            case SAFARI:
                if(osName.equals("mac")){
                    caps = DesiredCapabilities.safari();
                    caps.setBrowserName("safari");
                }
                break;
            case EDGE:
                if(osName.equals("windows")){
                    System.setProperty("webdriver.edge.driver", driverPath + "MicrosoftWebDriver.exe");
                    caps = DesiredCapabilities.edge();
                    caps.setBrowserName("edge");
                }
        }

        try {
            driver.set(new RemoteWebDriver(new URL(Constants.GRID_PORT), caps));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }catch (Exception e){
            System.err.println(e.getMessage());
        }
        int i = 10;

        for (int j = 1; j <= i; i++){
            try {
                driver.get().manage().window().maximize();
                break;
            }catch (WebDriverException we){
                driver.set(new ChromeDriver());
                driver.get().manage().window().maximize();
            }
            if (i == j){
                Assert.fail("Failed to maximize window " + j + " times");
            }
        }
        driver.get().manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        return driver.get();
    }


    public void removeDriver(){
        driver.get().quit();
        driver.remove();
    }
}
